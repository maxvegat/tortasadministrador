import { initializeApp } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js";
import { getDatabase, onValue, ref as refS, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.21.0/firebase-database.js";
const firebaseConfig = {
    apiKey: "AIzaSyAiWhhDBAZiL9wOHtggTLYTQ08BcenjGT8",
    authDomain: "administradorweb-8275e.firebaseapp.com",
    projectId: "administradorweb-8275e",
    storageBucket: "administradorweb-8275e.appspot.com",
    messagingSenderId: "87982913336",
    appId: "1:87982913336:web:7c12d4f58f37c36e6c33cd"
};
 
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
 
window.addEventListener('DOMContentLoaded', (event) => {
    ListarProductos();
    mostrarProductos();
  });
  
 
var btnAgregar = document.getElementById('btnAgregar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnBorrar');
 
var idTorta = "";
var nombreTorta = "";
var precio = "";
var url = "";
 
function leerInputs() {
  idTorta = document.getElementById('txtNumTorta').value;
  nombreTorta = document.getElementById('txtNombre').value;
  precio = document.getElementById('txtPrecio').value;
  url = document.getElementById('url').value;
}
 
function insertarDatos() {
  leerInputs();
  set(refS(db, 'productos/' + idTorta), {
    nombre: nombreTorta,
    Precio: precio,
    url: url
  }).then(() => {
    alert("Se insertó con éxito");
  }).catch((error) => {
    alert("Ocurrió un error: " + error);
  });
  limpiarInputs();
  ListarProductos();
}
 
function buscarDatos() {
  idTorta = document.getElementById('txtNumTorta').value;
  const dbRef = refS(db, 'productos/' + idTorta);
  get(dbRef).then((snapshot) => {
    if (snapshot.exists()) {
      const data = snapshot.val();
      nombreTorta = data.nombre;
      precio = data.Precio;
      url = data.url;
      escribirInputs();
    } else {
      alert("La Torta " + idTorta + " no existe");
    }
  }).catch((error) => {
    alert("Ocurrió un error: " + error);
  });
}
 
function escribirInputs() {
  document.getElementById('txtNombre').value = nombreTorta;
  document.getElementById('txtPrecio').value = precio;
  document.getElementById('url').value = url;
}
 
function ListarProductos() {
  const dbRef = refS(db, 'productos');
  const tabla = document.getElementById('tablaTortas');
  const tbody = tabla.querySelector('tbody');
 
  tbody.innerHTML = '';
 
  onValue(dbRef, (snapshot) => {
    snapshot.forEach((childSnapshot) => {
      const key = childSnapshot.key;
      const data = childSnapshot.val();
 
      var fila = document.createElement('tr');
 
      var celdaNumTorta = document.createElement('td');
      celdaNumTorta.textContent = key;
      fila.appendChild(celdaNumTorta);
 
      var celdaNombreTorta = document.createElement('td');
      celdaNombreTorta.textContent = data.nombre;
      fila.appendChild(celdaNombreTorta);
 
      var celdaPrecio = document.createElement('td');
      celdaPrecio.textContent = data.Precio;
      fila.appendChild(celdaPrecio);
 
      tbody.appendChild(fila);
    });
  }, { onlyOnce: true });
}
 
function actualizarDatos() {
  leerInputs();
  if (idTorta === "" || nombreTorta === "" || precio === "") {
    alert("Falta capturar información");
  } else {
    update(refS(db, 'productos/' + idTorta), {
      nombre: nombreTorta,
      Precio: precio,
      url: url
    }).then(() => {
      alert("Se actualizó con éxito");
      limpiarInputs();
    }).catch((error) => {
      alert("Ocurrió un error: " + error);
    });
  }
  ListarProductos();
}
 
function eliminarProducto() {
  idTorta = document.getElementById('txtNumTorta').value;
  remove(refS(db, 'productos/' + idTorta)).then(() => {
    alert("Producto eliminado con éxito");
    limpiarInputs();
    ListarProductos();
  }).catch((error) => {
    alert("Ocurrió un error al eliminar el producto: " + error);
  });
}
 
function limpiarInputs() {
  document.getElementById('txtNumTorta').value = '';
  document.getElementById('txtNombre').value = '';
  document.getElementById('txtPrecio').value = '';
  document.getElementById('url').value = '';
}


document.getElementById("btnVolver").addEventListener("click", () => {
  window.location.href = "/html/index.html";
});
  
 
btnBorrar.addEventListener('click', eliminarProducto);
btnAgregar.addEventListener('click', insertarDatos);
btnActualizar.addEventListener('click', actualizarDatos);
btnBuscar.addEventListener('click', buscarDatos);