import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-app.js';
import { getAuth, signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.21.0/firebase-auth.js';

const firebaseConfig = {
    apiKey: "AIzaSyAiWhhDBAZiL9wOHtggTLYTQ08BcenjGT8",
    authDomain: "administradorweb-8275e.firebaseapp.com",
    projectId: "administradorweb-8275e",
    storageBucket: "administradorweb-8275e.appspot.com",
    messagingSenderId: "87982913336",
    appId: "1:87982913336:web:7c12d4f58f37c36e6c33cd"
};


const firebaseApp = initializeApp(firebaseConfig);
const auth = getAuth(firebaseApp);


const formulario = document.querySelector("form");
const emailInput = document.getElementById("txtCorreo");
const passwordInput = document.getElementById("txtContraseña");
const signInButton = document.querySelector("button");
const errorMensaje = document.getElementById("errorMensaje");

formulario.addEventListener("submit", (event) => {
  event.preventDefault();

  const email = emailInput.value;
  const password = passwordInput.value;

  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      window.location.href = "/html/admin.html";
    })
    .catch((error) => {
      console.log("Error al iniciar sesión:", error);
      errorMensaje.textContent = "Credenciales incorrectas. Por favor, intenta nuevamente.";
    });
});

emailInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

passwordInput.addEventListener("click", () => {
  errorMensaje.textContent = "";
});

